<?php

/**
 * 
 */
class Home extends CI_Controller
{
	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('home/index');
		$this->load->view('templates/footer');
	}

	public function about()
	{
		$this->load->view('templates/header');
		$this->load->view('home/about');
		$this->load->view('templates/footer');
		}

		public function price()
	{
		$this->load->view('templates/header');
		$this->load->view('home/price');
		$this->load->view('templates/footer');
		}

		public function login()
	{
		$this->load->view('templates/header');
		$this->load->view('home/login');
		$this->load->view('templates/footer');
		}

	}