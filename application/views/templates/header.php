<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assests/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assests/css/about.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>E-Counseling - Konseling Online </title>
    <style type="text/css">
      
      .jumbotron{
        background-image: url(assests/img/home-hero.png);
      }
      .jumbotron1{
        background-image: url(assests/img/price/tew2.jpg);
      }
    </style>
  </head>
  <body>

       <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
      <div class="container">
      <a class="navbar-brand" href="<?= base_url(); ?> "><img class="logo" src="assests/img/logo.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link" href="<?php echo base_url('Home/about'); ?> ">About Us</a>
          <a class="nav-item nav-link" href="<?php echo base_url('Home/price'); ?>">Price</a>
          <a class="nav-item nav-link " href="#">Blog</a>
          <a class="nav-item nav-link " href="#">FAQ</a>
        </div>
        </div>
        </div>
      </nav>
